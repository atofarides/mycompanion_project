package uk.co.deloittedigital.mashup.mycompanion.service;

import org.codehaus.jettison.json.JSONObject;
import uk.co.deloittedigital.mashup.mycompanion.model.Recommendation;

/**
 * Created by kwood on 21/11/14.
 */
public interface WithingsService {

    public JSONObject getData();

    public Recommendation getRecommendation(JSONObject jsonObject);
}
