package uk.co.deloittedigital.mashup.mycompanion.model;

/**
 * Created by kwood on 21/11/14.
 */
public class OAUTHUser {
    private String consumerKey;
    private String consumerSecret;
    private String userId;
    private String token;
    private String tokenSecret;

    public OAUTHUser(String consumerKey, String consumerSecret, String userId, String oauthToken, String tokenSecret) {
        this.consumerKey = consumerKey;
        this.consumerSecret = consumerSecret;
        this.userId = userId;
        this.token = oauthToken;
        this.tokenSecret = tokenSecret;
    }

    public String getConsumerKey() {
        return consumerKey;
    }

    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }

    public String getConsumerSecret() {
        return consumerSecret;
    }

    public void setConsumerSecret(String consumerSecret) {
        this.consumerSecret = consumerSecret;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokenSecret() {
        return tokenSecret;
    }

    public void setTokenSecret(String tokenSecret) {
        this.tokenSecret = tokenSecret;
    }
}
