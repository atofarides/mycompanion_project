package uk.co.deloittedigital.mashup.mycompanion.example.controller;

import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import uk.co.deloittedigital.mashup.mycompanion.example.model.Person;
import uk.co.deloittedigital.mashup.mycompanion.example.service.PersonService;
import uk.co.deloittedigital.mashup.mycompanion.model.Recommendation;
import uk.co.deloittedigital.mashup.mycompanion.service.WithingsService;
import uk.co.deloittedigital.mashup.mycompanion.service.impl.WithingsServiceImpl;

import java.util.Map;

@Controller
public class PersonController {

    @Autowired
    private PersonService personService;

    @RequestMapping("/")
    public String getData(Map<String, Object> map) {
        WithingsService withingsService = new WithingsServiceImpl();

        JSONObject data = withingsService.getData();
        Recommendation recommendation = withingsService.getRecommendation(data);

        return "";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addPerson(@ModelAttribute("person") Person person, BindingResult result) {

        personService.addPerson(person);

        return "redirect:/people/";
    }

    @RequestMapping("/delete/{personId}")
    public String deletePerson(@PathVariable("personId") Integer personId) {

        personService.removePerson(personId);

        return "redirect:/people/";
    }
}
