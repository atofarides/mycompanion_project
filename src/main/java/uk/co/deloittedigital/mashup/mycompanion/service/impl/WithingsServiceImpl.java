package uk.co.deloittedigital.mashup.mycompanion.service.impl;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.joda.time.DateTime;
import uk.co.deloittedigital.mashup.mycompanion.model.OAUTHUser;
import uk.co.deloittedigital.mashup.mycompanion.model.Recommendation;
import uk.co.deloittedigital.mashup.mycompanion.service.WithingsService;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by kwood on 21/11/14.
 */
public class WithingsServiceImpl implements WithingsService {
    private final OAUTHUser Kamal = new OAUTHUser("156808e7281216c1f9051818164fb056b3b96814f8bb1f979f40f26cb156",
            "e8fd1555d0271e1d99758e5d8cb36e0b175c60eb8b1b2390f3e3e110019b4f",
            "5376172", "f0cfe75661c9817850ec6de31fd933848c07603164f4c15156c894ed6d6f",
            "fcc5981e6b3f46510cef52215d66278c55cc88c1dcb86bfcc2077d48f15");
    private final OAUTHUser Antonios = new OAUTHUser("156808e7281216c1f9051818164fb056b3b96814f8bb1f979f40f26cb156",
            "e8fd1555d0271e1d99758e5d8cb36e0b175c60eb8b1b2390f3e3e110019b4f","5376873",
            "455537b095311cd155bde26ddf095c3f7d74ec29580782e2ce505128576f8f4",
            "10d5253d38900d82027b8f93c589913076f021900e63bd9390e89f26e0d659");

//    http://wbsapi.withings.net/measure?action=getmeas&oauth_consumer_key=&oauth_nonce=&oauth_signature=7Qrlqhp3Fhbiv2h0AZpwbXN23G8%3D&oauth_signature_method=HMAC-SHA1&oauth_timestamp=1416586292&oauth_token=455537b095311cd155bde26ddf095c3f7d74ec29580782e2ce505128576f8f4&oauth_version=1.0&userid=5376873

    public static Map<String,String> MEASURE_TYPE_MEANING = new HashMap<String,String>();
    static {
        MEASURE_TYPE_MEANING.put("11","Heart Pulse(bpm)");
        MEASURE_TYPE_MEANING.put("54","SP02(%)");
    }

    @Override
    public JSONObject getData() {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        URI uri = null;
        try {
            URIBuilder getURI = new URIBuilder()
                    .setScheme("http")
                    .setHost("wbsapi.withings.net")
                    .setPath("/measure")
                    .setParameter("action", "getmeas")
                    .setParameter("oauth_consumer_key", "156808e7281216c1f9051818164fb056b3b96814f8bb1f979f40f26cb156")
                    .setParameter("oauth_nonce", "248bdb58ae8872ac3b4027d11af4bd9a")
                    .setParameter("oauth_signature", "7Qrlqhp3Fhbiv2h0AZpwbXN23G8=")
                    .setParameter("oauth_signature_method", "HMAC-SHA1")
                    .setParameter("oauth_timestamp", "1416586292")
                    .setParameter("oauth_token", "455537b095311cd155bde26ddf095c3f7d74ec29580782e2ce505128576f8f4")
                    .setParameter("oauth_version", "1.0")
                    .setParameter("userid", "5376873");
            uri = getURI.build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
        HttpGet httpGet = new HttpGet(uri);

        JSONObject jsonObj = null;
        try {
            CloseableHttpResponse response = httpClient.execute(httpGet);
            HttpEntity httpEntity = response.getEntity();

            if (httpEntity != null) {
                // TODO: Apparently EntityUtils is REALLY unsafe!
                String entityString = EntityUtils.toString(httpEntity);
//                System.out.println(entityString);
                jsonObj = new JSONObject(entityString);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObj;
    }

    @Override
    public Recommendation getRecommendation(JSONObject jsonObject) {
        try {
            if (jsonObject != null) {

                JSONObject body = jsonObject.getJSONObject("body");
                if(body != null) {
                    JSONArray measureGroups = body.getJSONArray("measuregrps");
                    JSONObject latestMeasureGroup = null;
                    String bpm = "";
                    String oxygen = "";
                    if(measureGroups != null) {
                        for(int j = 0; j < measureGroups.length(); j++) {
                            bpm = "";
                            oxygen = "";
                            latestMeasureGroup = (JSONObject) measureGroups.get(j);
                            JSONArray measureValues = latestMeasureGroup.getJSONArray("measures");

                            if (measureValues != null) {
                                for (int i = 0; i < measureValues.length(); i++) {
                                    JSONObject measureValue = measureValues.getJSONObject(i);
                                    if (measureValue.getInt("type") == 11) {
                                        bpm = measureValue.getString("value");
                                    } else if (measureValue.getInt("type") == 54) {
                                        oxygen = measureValue.getString("value");
                                    }
                                }
                                if(!"".equals(bpm) && !"".equals(oxygen)) {
                                    break;
                                }
                            }
                        }

                        String scenario = getScenarioFromDateTime(latestMeasureGroup.getInt("date"));
                        Recommendation recommendation = new Recommendation(scenario, bpm, oxygen);
                        populateRecommendationWithStateAndRecommenedSong(recommendation);
                        return recommendation;
                    }
                }
            } else {
                return null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private String getScenarioFromDateTime(int unixTime) {
        String scenario;
        DateTime dateTime = new DateTime(unixTime * 1000L);

        int hourOfDay = dateTime.getHourOfDay();
        if(hourOfDay >= 9 && hourOfDay <= 18) {
            scenario = "Work";
        } else if (hourOfDay > 18) {
            scenario = "Date";
        } else {
            scenario = "Other";
        }

        return scenario;
    }

    private void populateRecommendationWithStateAndRecommenedSong(Recommendation recommendation) {
        Integer bpm = Integer.parseInt(recommendation.getBpm());

        if(bpm < 70) {
            recommendation.setState("Bored");
            if(recommendation.getScenario().equals("Work")) {
                recommendation.setRecommendedSong("boredwork");
            } else if (recommendation.getScenario().equals("Date")) {
                recommendation.setRecommendedSong("boreddate");
            } else {
                recommendation.setRecommendedSong("boredother");
            }
        } else if (bpm < 90) {
            recommendation.setState("Neutral");
            if(recommendation.getScenario().equals("Work")) {
                recommendation.setRecommendedSong("neutralwork");
            } else if (recommendation.getScenario().equals("Date")) {
                recommendation.setRecommendedSong("neutraldate");
            } else {
                recommendation.setRecommendedSong("neutralother");
            }
        } else if (bpm >= 90) {
            if(recommendation.getScenario().equals("Work")) {
                recommendation.setState("Stressed");
                recommendation.setRecommendedSong("stressedwork");
            } else if (recommendation.getScenario().equals("Date")) {
                recommendation.setState("Aroused");
                recommendation.setRecommendedSong("arouseddate");
            } else {
                recommendation.setState("Stressed");
                recommendation.setRecommendedSong("stressedother");
            }
        }
    }
}
