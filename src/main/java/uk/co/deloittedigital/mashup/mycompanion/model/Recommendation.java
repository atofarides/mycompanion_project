package uk.co.deloittedigital.mashup.mycompanion.model;

/**
 * Created by kwood on 21/11/14.
 */
public class Recommendation {
    private String scenario;
    private String state;
    private String bpm;
    private String oxygen;
    private String recommendedSong;

    public Recommendation(String scenario, String bpm, String oxygen) {
        this.scenario = scenario;
        this.bpm = bpm;
        this.oxygen = oxygen;
    }

    public String getScenario() {
        return scenario;
    }

    public void setScenario(String scenario) {
        this.scenario = scenario;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getBpm() {
        return bpm;
    }

    public void setBpm(String bpm) {
        this.bpm = bpm;
    }

    public String getOxygen() {
        return oxygen;
    }

    public void setOxygen(String oxygen) {
        this.oxygen = oxygen;
    }

    public String getRecommendedSong() {
        return recommendedSong;
    }

    public void setRecommendedSong(String recommendedSong) {
        this.recommendedSong = recommendedSong;
    }
}
