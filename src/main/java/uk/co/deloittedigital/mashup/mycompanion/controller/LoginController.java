package uk.co.deloittedigital.mashup.mycompanion.controller;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import uk.co.deloittedigital.mashup.mycompanion.model.Recommendation;
import uk.co.deloittedigital.mashup.mycompanion.service.WithingsService;
import uk.co.deloittedigital.mashup.mycompanion.service.impl.WithingsServiceImpl;

/**
 * Created by atofarides on 21/11/2014.
 */
@Controller
public class LoginController {

    @RequestMapping(value = "/login", produces = "application/json")
    public @ResponseBody String data() {

        WithingsService withingsService = new WithingsServiceImpl();

        JSONObject data = withingsService.getData();
        Recommendation recommendation = withingsService.getRecommendation(data);

        JSONObject result = new JSONObject();
        try {
            result.put("scenario", recommendation.getScenario());
            result.put("state", recommendation.getState());
            result.put("bpm", recommendation.getBpm());
            result.put("oxygen", recommendation.getOxygen());
            result.put("recommendedSong", recommendation.getRecommendedSong());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return result.toString();
    }
}
