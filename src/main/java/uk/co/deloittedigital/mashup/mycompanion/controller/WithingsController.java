package uk.co.deloittedigital.mashup.mycompanion.controller;

import uk.co.deloittedigital.mashup.mycompanion.service.WithingsService;
import uk.co.deloittedigital.mashup.mycompanion.service.impl.WithingsServiceImpl;

/**
 * Created by kwood on 21/11/14.
 */
public class WithingsController {
//    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String requestData() {
        WithingsService withingsService = new WithingsServiceImpl();

        withingsService.getData();

        return "";
    }
}
