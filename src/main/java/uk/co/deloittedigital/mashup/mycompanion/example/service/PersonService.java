package uk.co.deloittedigital.mashup.mycompanion.example.service;


import java.util.List;

import uk.co.deloittedigital.mashup.mycompanion.example.model.Person;

public interface PersonService {
    
    public void addPerson(Person person);
    public List<Person> listPeople();
    public void removePerson(Integer id);
}
